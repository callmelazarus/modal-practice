
# Journal


## Mar 21, 2023 - Tue

**Today I worked on:**
- bio modal (desktop and mobile)

**Reflection:**
- finding in dev tools, the correct element to select to provide the right id for 'getElementbyId'
- needed to mess with modal for mobile view
- font type vh is very helpful for responsive websites, as the font size will scale based on the size of the screen. There is `vh` which si for vertical scaling, and `vw` which is for width scaling
- got a laugh from the challenge in centering a div...

**Lesson:**
- Turning off the link on the photo fixes things as well
- setting something as an ID will effect things differently... for example. I am trying to change the modal body font to be responsive based on the screen size. I would use `font-size: 1vh` with a class selector `.` and it wouldn't work. when I changed it to a `id` attribute instead, and selected it, it works!!!




## March 8, 2023 - Wed

**Today I worked on:**
- worked with Jonathan to create two buttons for modal. one button for mobile view (redirect), and another button for desktop (modal)
- when clicking on modal - prevent screen from scrolling (sticky)
**Reflection:**
**Lesson:**



## Feb 24, 2023 - Friday

**Today I worked on:**
- started html / css / javascript files for modal
- Created a video.assist
**Reflection:**
**Lesson:**
reference CSS - using (inside the head tag)
<link rel="stylesheet" href="styles.css">

The location of the script matters. When I placed the embedded code block into 

YT embeded urls, need to have the 'embed' path. Having `?enablejsapi=1` also is important for having the pause functionality

---