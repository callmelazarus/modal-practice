# Making Modals with HTML/CSS/Javascript

GOAL:
Practice creating my own modal with HTML / CSS / and JS
In order to embed a Youtube video in the modal. This was done as an overal exercise, and then to apply a modal within Squarespace.

### End Result:

On Desktop:

<img src="/assets/modal.gif" alt="modalDesktop" width="800"/>

On Mobile:

<img src="/assets/modalmobile.gif" alt="modalmobile" width="200"/>



Resource:
https://www.w3schools.com/howto/howto_css_modals.asp

1. Decided to group all code into one file

2. Learned the importance of ending the URL with `?enablejsapi=1` - which allows me to pause the video when people click on the 'x' button, or on the window

3. Added animation for the modal to make it more interesting
4. Styled Modal to match style guide

### Steps to update youtube video

1. Find Youtube video you want to use
2. Click on share button, and then the embed button
   1. copy the url of the embedded link (its very similar to normal url, but with embed in the path)
3. Go to the Code `iframe` tag, the `src` attribute
   1. keep the last portion of the url`?enablejsapi=1`
   2. Replace the portion of the URL before that `?enable...` with the embed URL from YT
   3. At the end, it will look something like this
      `src="https://www.youtube.com/embed/FdJ-etzzynE?enablejsapi=1"`
   - note the " quotes around the URL string

### Steps for setting up FontAwesome Icon

1. Insert custom `script` into header
   `<script src="https://kit.fontawesome.com/0e73e9997c.js" crossorigin="anonymous"></script>`
2. Insert `<i>` tag for icon usage

### How this modal is setup on Squarespace


1. Install fontawesome script in page specific CSS
   `<script src="https://kit.fontawesome.com/0e73e9997c.js" crossorigin="anonymous"></script>`

2. Insert code block to create FontAwesome Icon - HTML. Adjust size to taste. Set id to query later.
   `<i class="fa-sharp fa-regular fa-circle-play fa-4x" id="newBtn"></i>`

3. Insert Page specific CSS for the page in question - that styles the modal.
4. Add code block with modal HTML and JS `<script> tag` (set as HTML setting in Squarespace), and hide the code. This code block needs to be below where the font awesome icon/button is located.

### Setup Modal for bio information
- Similar to modal for video.
- Challenge is identifying the specific image, and selecting the correct image on the site
